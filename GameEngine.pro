# Add files and directories to ship with the application 
# by adapting the examples below.
# file1.source = myfile
# dir1.source = mydir
DEPLOYMENTFOLDERS = # file1 dir1


# Uncomment/comment the following line to enable/disable debug prints of
# Qt GameEnabler audio framework.
#DEFINES += GE_DEBUG

# If your application uses the Qt Mobility libraries, uncomment
# the following lines and add the respective components to the
# MOBILITY variable.

QT += multimedia core gui opengl
CONFIG += mobility
MOBILITY += systeminfo \
            multimedia

#after having these, create a folder resourc and add to it all the images, sounds that you want to use in your game

MOC_DIR = moc
OBJECTS_DIR = obj
unix:!symbian:!maemo5:!win {
    #Special case for N950 that works, all other devices will use the else case
    dir1.files = resourc/*
    dir1.path  = /opt/
#$${TARGET}
    INSTALLS += dir1
} else {
    dir1.source += resourc/*
    DEPLOYMENTFOLDERS = dir1
}

unix {
    mac {
        CONFIG -= app_bundle
    }
}

symbian:TARGET.UID3 = 0xE2886576

# Smart Installer package's UID
# This UID is from the protected range 
# and therefore the package will fail to install if self-signed
# By default qmake uses the unprotected range value if unprotected UID is defined for the application
# and 0x2002CCCF value if protected UID is given to the application
#symbian:DEPLOYMENT.installer_header = 0x2002CCCF

# Allow network access on Symbian
symbian:TARGET.CAPABILITY += NetworkServices

# If your application uses the Qt Mobility libraries, uncomment
# the following lines and add the respective components to the 
# MOBILITY variable. 
# CONFIG += mobility
# MOBILITY +=

SOURCES += main.cpp \
    Srcs/transitionscene.cpp \
    Srcs/textitem.cpp \
    Srcs/spriteitem.cpp \
    Srcs/sprite.cpp \
    Srcs/sequence.cpp \
    Srcs/scene.cpp \
    Srcs/node.cpp \
    Srcs/menuitemlabel.cpp \
    Srcs/menuitemimage.cpp \
    Srcs/menuitem.cpp \
    Srcs/menu.cpp \
    Srcs/layer.cpp \
    Srcs/label.cpp \
    Srcs/graphicsscene.cpp \
    Srcs/director.cpp \
    Srcs/audiomanager.cpp \
    Srcs/action.cpp \
    main_menu.cpp \
    Srcs/GEInterfaces.cpp \
    Srcs/GEAudioOut.cpp \
    Srcs/GEAudioMixer.cpp \
    Srcs/GEAudioBufferPlayInstance.cpp \
    Srcs/GEAudioBuffer.cpp
HEADERS += \
    Srcs/transitionscene.h \
    Srcs/textitem.h \
    Srcs/spriteitem.h \
    Srcs/sprite.h \
    Srcs/sequence.h \
    Srcs/scene.h \
    Srcs/node.h \
    Srcs/myfastergraphicview.h \
    Srcs/menuitemlabel.h \
    Srcs/menuitemimage.h \
    Srcs/menuitem.h \
    Srcs/menu.h \
    Srcs/layer.h \
    Srcs/label.h \
    Srcs/graphicsscene.h \
    Srcs/global_keys.h \
    Srcs/game_engine.h \
    Srcs/game_config.h \
    Srcs/director.h \
    Srcs/audiomanager.h \
    Srcs/action.h \
    main_menu.h \
    Srcs/trace.h \
    Srcs/GEInterfaces.h \
    Srcs/GEAudioOut.h \
    Srcs/GEAudioMixer.h \
    Srcs/GEAudioBufferPlayInstance.h \
    Srcs/GEAudioBuffer.h
FORMS +=

# Please do not modify the following two lines. Required for deployment.
include(deployment.pri)
qtcAddDeployment()

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog \
    qtc_packaging/debian_fremantle/rules \
    qtc_packaging/debian_fremantle/README \
    qtc_packaging/debian_fremantle/copyright \
    qtc_packaging/debian_fremantle/control \
    qtc_packaging/debian_fremantle/compat \
    qtc_packaging/debian_fremantle/changelog













