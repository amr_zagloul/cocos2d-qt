#include "spriteitem.h"
#include <QGraphicsSceneMouseEvent>
#include <QDebug>
#include <QBitmap>
#include "graphicsscene.h"
#include <QTransform>

spriteItem::spriteItem(const QString &filename,bool button,const QString &clickedFileName) {
    bool load = QPixmapCache::find(filename,&normal);
    if (!load) {
        load = normal.load(filename);
        if (load) {
            QPixmapCache::insert(filename,normal);
        } else {
            qDebug() << "Game Engine :: File \"" << filename << "\" is not successfuly loaded, make sure file exists";
        }
    }
    if (button) {
        if (clickedFileName == "") {
            if (load) {
                clicked.load(filename);
                clicked.setMask(clicked.createMaskFromColor(QColor(0,0,0,100)));
            }
        }
        else {
            bool load = QPixmapCache::find(clickedFileName,&clicked);
            if (!load) {
                load = clicked.load(clickedFileName);
                if (load) {
                    QPixmapCache::insert(clickedFileName,clicked);
                } else {
                    qDebug() << "Game Engine :: File \"" << clickedFileName << "\" is not successfuly loaded, make sure file exists";
                }
            }
        }
    }
    setPixmap(normal);
    setTransformationMode(Qt::SmoothTransformation);
    parentMenuItem = 0;
    setFlag(QGraphicsItem::ItemSendsGeometryChanges,0);
    setFlag(QGraphicsItem::ItemIsMovable,0);
    setFlag(QGraphicsItem::ItemIsSelectable,0);
    setFlag(QGraphicsItem::ItemIsFocusable,0);
    setFlag(QGraphicsItem::ItemIgnoresTransformations,0);
    setFlag(QGraphicsItem::ItemIgnoresParentOpacity,0);
    setFlag(QGraphicsItem::ItemAcceptsInputMethod,0);
    setFlag(QGraphicsItem::ItemNegativeZStacksBehindParent,0);
    setEnabled(0);
    setCacheMode(QGraphicsItem::NoCache);
}
spriteItem::~spriteItem() {

}

qreal spriteItem::height() {
    return this->pixmap().height();
}
qreal spriteItem::width() {
    return this->pixmap().width();
}


void spriteItem::setParent(MenuItem *parent) {
    parentMenuItem = parent;
}

