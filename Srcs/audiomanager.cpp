#include "audiomanager.h"
/*
Refer to
https://projects.developer.nokia.com/qtgameenabler/wiki/audio
for more info, please use wav 22050 kHz, 2 channels, signed 16 bit PCM.
*/

GE::AudioOut * audioManager::m_audioOut;
GE::AudioMixer audioManager::m_mixer;
#ifdef Q_OS_SYMBIAN
QTimer audioManager::m_audioPullTimer;
#endif

audioManager::audioManager(QObject *parent) :
    QObject(parent)
{

}

void audioManager::playSound(QString path) {
    GE::AudioBuffer* m_someSample = GE::AudioBuffer::loadWav("resource/"+path);
    GE::AudioBufferPlayInstance *m_instance = new GE::AudioBufferPlayInstance(m_someSample);
    connect(m_instance,SIGNAL(finished()),m_someSample,SLOT(deleteLater()));
//    connect(m_instance,SIGNAL(finished()),m_instance,SLOT(deleteLater()));
    m_mixer.addAudioSource(m_instance);
}

void audioManager::setUpAudioManager() {
    m_audioOut = new GE::AudioOut(&m_mixer);
#ifdef Q_OS_SYMBIAN
    m_audioPullTimer.setInterval(5);
    connect(&m_audioPullTimer, SIGNAL(timeout()), m_audioOut, SLOT(tick()));
#endif
    audioManager::enableSounds(true);

}

void audioManager::enableSounds(bool enable)
{
    if (enable) {
#ifdef Q_OS_SYMBIAN
        m_audioPullTimer.start();
#endif
        m_mixer.setGeneralVolume(0.2f);
    }
    else {
#ifdef Q_OS_SYMBIAN
        m_audioPullTimer.stop();
#endif
        m_mixer.setGeneralVolume(0);
    }
}
