#include <QtGui/QApplication>
#include <QDir>
//here's the first one to change
#include "main_menu.h"
#include "Srcs/game_engine.h"


int main(int argc, char *argv[])
{
    QApplication::setGraphicsSystem("raster");
    QApplication app(argc, argv);
    //this code, until the last QDir is to set the CWD, aka Current Working Directory
    QDir dir(QCoreApplication::applicationDirPath());
    if (dir.dirName().toLower() == "bin")
    {
        dir.cdUp();
    }
#ifdef WIN32
    if (dir.dirName().toLower() == "debug" || dir.dirName().toLower() == "release")
    {
        dir.cdUp();
    }
#endif
    QDir::setCurrent(dir.absolutePath());
    //set up audio playback
    audioManager::setUpAudioManager();
    //here is the second main_menu to change
    Director::sharedDirector()->startWithScene(main_menu::scene());
    return app.exec();
}
